const manageData = (state, action) => {
    'use strict';
    switch (action.type) {
        case 'SET_INPUT_WORDS':
            return {
                ...state,
                input_words: action.payload
            };
        case "ADD_RESPONSE": {
            return {
                ...state,
                result_words: [...state.result_words, action.payload]
            };
        }
        case "NEW_GAME":
            return {
                ...state,
                result_words: [], input_words: []
            };
        case "STORE_LEADERBOARD":
            return {
                ...state,
                leaderboard: action.payload
            };
        default:
            return {...state}
    }
};
export default manageData;
