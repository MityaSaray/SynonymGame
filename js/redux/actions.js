import axios from 'axios'
import shuffle from '../logic/shuffle'
axios.defaults.crossDomain = true;

const leaderboard = board => {
    return {
        type: "STORE_LEADERBOARD",
        payload: board
    }
};
const inputWords = words => {
    return {
        type: "SET_INPUT_WORDS",
        payload: words
    }
};
//we return promise from our action, so we can use .then() functionality when rerouting
function makeReqAndPush(el) {
    return dispatch => axios.get('https://api.datamuse.com/words', {
        params: {ml: el},
        timeout: 5000
    }).then((result) => {
        dispatch({type: 'ADD_RESPONSE', payload: {data: shuffle(result.data), word: el}})
    }).catch((error) => {
        if (error.response) {
            console.log(error.response.data);
            console.log(error.response.status);
            console.log(error.response.headers);
        }
        else if (error.request) {
            console.log(error.request)
        }
        else {
            console.log('Error', error.message)
        }
        console.log(error.config)
    })

}

const newGame = words => {
    return {
        type: "NEW_GAME",
        payload: words
    }
};
export {inputWords, makeReqAndPush, newGame, leaderboard}