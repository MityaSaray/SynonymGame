import {createStore, applyMiddleware} from 'redux'
import manageData from './redux/reducers'
import ReduxThunk from 'redux-thunk'

const store = createStore(manageData, applyMiddleware(ReduxThunk));
store.subscribe(()=>{console.log(store.getState())});
export default store;