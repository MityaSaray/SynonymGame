import {BrowserRouter as Router, Route} from 'react-router-dom'
import {Switch} from 'react-router-dom'
import React from 'react'
import {Provider} from 'react-redux'
import InputView from './views/InputView'
import Output from './views/outputView'
const MainRouter = ({store}) => (
    <Provider store={store}>
        <Router>
            <Switch>
                <Route exact path='/' component={InputView}/>
                <Route path={'/res/'} component={Output}/>
            </Switch></Router></Provider>
);
export default MainRouter;