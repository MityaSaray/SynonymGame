import React from 'react'
import axios from 'axios'
import {withRouter} from 'react-router'
import ModalWin from './ModalWindow'

class Timer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {start_time: new Date(), elapsed_time: 0};
        this.tick = this.tick.bind(this);
        this.timer = setInterval(this.tick, 1);
    }

    componentWillUnmount() {
        clearInterval(this.timer)
    }
    //our timer function
    tick = () => {
        this.setState({elapsed_time: new Date() - this.state.start_time})
    };
    setScore = (name) => {
        if (name.length > 0) {
            //sends data to leaderboard and redirects to first page.
            axios.post(window.location.pathname + 'result/', {
                username: JSON.stringify(name),
                diff: JSON.stringify(this.props.difficulty),
                time: JSON.stringify(this.state.elapsed_time)
            }).then(() => {
                this.props.history.replace('/')
            })
        }
        //if empty score we redirect to first page without sending data
        else {
            this.props.history.replace('/')
        }
    };

    render() {
        let elapsed_shown = (this.state.elapsed_time / 1000).toFixed(1);
        //our timer
        if (this.props.running === true) {
            return <div className={'timer'}><span className={'center'}><h4>Time is running {elapsed_shown}</h4></span>
            </div>
        }
        //when timer stops we display modal window
        else {
            clearInterval(this.timer);
            return <ModalWin setSc={this.setScore.bind(this)} diff={this.props.difficulty}
                             time={this.state.elapsed_time}/>
        }
    }
}

export default withRouter(Timer);