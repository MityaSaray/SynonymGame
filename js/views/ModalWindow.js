import React from 'react'

class ModalWin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {name: ""}
    }

    setInput(event) {
        //handle input
        this.setState({name: event.target.value})
    }

    render() {
        //we create Style properties for out Modal Window
        const background = {
            position: 'fixed',
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            backgroundColor: 'rgba(0,0,0,0.3)',
            padding: 50
        };
        const modalStyle = {
            borderRadius: 5,
            maxWidth: 500,
            minHeight: 300,
            margin: '0 auto',
            padding: 30
        };
        return <div>
            <div style={background}>
                <div style={modalStyle} className={'bg-dark text-center'}><h1>Congratulations!</h1>
                    <p>Your result is {(this.props.time/1000).toFixed(3)} seconds with difficulty {this.props.diff}</p>
                    <div className={'h-50 justify-content-center mt-5'}><input onChange={(event) => {
                        this.setInput(event)
                    }}/><h3>Enter your name, or leave space empty to go home</h3>
                        <button type={'button'} className={'btn btn-light'} onClick={() => {
                            //using our parents function and passing input there
                            this.props.setSc(this.state.name)
                        }}>Sumbit Result
                        </button>
                    </div>
                </div>
            </div>
        </div>

    }
}

export default ModalWin
