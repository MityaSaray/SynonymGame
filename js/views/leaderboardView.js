import React from 'react';
import axios from 'axios';

class Leaderboard extends React.Component {
    constructor(props) {
        super(props);
        //we are setting dV(difficulty displayed) array to list all difficulties we want to display in leaderboard.
        this.state = {lb: [], dV: [3, 4, 5, 6, 7]};
    }

    componentDidMount() {
        this.getLeaderboard()

    }

    getLeaderboard() {
        let temp = this.state.lb;
        this.state.dV.map((el) => {
            axios.get(window.location.pathname + 'leaderboard?' + 'diff=' + el)
                .then((response) => {
                    temp.push(response.data);
                    this.setState({lb: temp});
                })
                .catch((response) => ((console.log(response)) + 'problem is here'));
        });
    }

    render() {
        if (this.state.dV.length === this.state.lb.length) {
            return <div className={'row'}>
                <div className={'col justify-content-center'}>
                    <h1 className={'text-center'}>Leaderboard</h1>{this.state.dV.map((el, i) => {
                    return <div key={'div' + i} className={'text-center'}>
                        <button type={'button'} className={'btn btn-light mt-2'} data-toggle={'collapse'}
                                data-target={'#diff' + el}>
                            Difficulty {el} results
                        </button>
                        <div id={'diff' + el} className={'collapse'}>
                            <table className={'table table-condensed text-light'}>
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Time</th>
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    this.state.lb[i].map((el) => {
                                        return <tr key={el.time}>
                                            <th>{el.username}</th>
                                            <th>{el.time}</th>
                                        </tr>
                                    })
                                }
                                </tbody>
                            </table>
                        </div>
                    </div>
                })}</div>
            </div>
        } else {
            return <h1>Loading</h1>
        }
    }
}

export default Leaderboard;
