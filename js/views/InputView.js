import React from 'react';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import * as actions from '../redux/actions';
import {bindActionCreators} from 'redux';
import shuffle from "../logic/shuffle";
import Leaderboard from './leaderboardView';

class InputView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {words_input: ""};
        //clearing redux state on new game and setting local state from database request.
        this.props.dispatch(actions.newGame());
    }

    gameStart() {
        //function that is called on click of start game button. Splits input, removes empty arrays, dispatches input
        //to redux state. Shuffle input and dispatch action that performs get requests from datamuse. On last call
        //pushed history props.

        let array = this.state.words_input.split(" ");
        //filter our empty arrays
        let array1 = array.filter(el => {
            return el !== ""
        });
        //populate redux input state and shuffle
        this.props.dispatch(actions.inputWords(array1));
        array1 = shuffle(array1);
        //we perform async HTTP requests and when all are done we proceed to next page
        array1.forEach((el, i) => {
                this.props.dispatch(actions.makeReqAndPush(el)).then(() => {
                    if (i === array1.length - 1) {
                        this.props.history.push("/res/");
                    }
                });
            }
        );
    }

    render() {
        return <div className={'row minheight'}>
            <div className={"col-2 bg-dark text-light w-50 pt-3 text-center pt-5"}><h1>GameRules</h1>
                <p>Enter words in English, then when you push button you will be presented with output of words that
                    are connected with your input words in a shuffled order.</p>
                <p> You will need to match word to these synonyms. Difficulty is measured by amount of words you used
                    and timer starts as soon as you press the
                    button.</p>
                <p><code>Make love not war</code> is a good example.</p>
                <p>Have fun!
                </p></div>
            <div className={"container col-7 bg-light pt-5"}><h1>Synonym Game</h1>
                <label htmlFor="input"> Enter your words!</label>
                <input type='text' id='input' className={'form-control'} aria-describedby={'helpblock'}
                       onChange={(event) => {
                           this.setState({words_input: event.target.value})
                       }}></input>
                <small id='helpblock' className={'form-text text-muted pt-5'}>Enter words in format "word anotherword"
                </small>
                <button type='button' id='getWords' className={'btn btn-dark btn-block'} onClick={
                    this.gameStart.bind(this)
                }>Start the game
                </button>
            </div>
            <div className={'bg-dark text-light col-3 pt-5 pr-2'}><Leaderboard/></div>
        </div>
    }
}

const mapDispatchToProps = dispatch => {
    return {actions: bindActionCreators(actions, dispatch)}
};
export default withRouter(connect(mapDispatchToProps)(InputView));