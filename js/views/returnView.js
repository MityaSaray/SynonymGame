import React from 'react'
import {withRouter} from 'react-router'

class ReturnView extends React.Component {
    constructor(props) {
        super(props);
        this.handlereturn.bind(this)
    }

    handlereturn = () => {
    this.props.history.replace('/')
    };

    render() {
        if (Array.isArray(this.props.shuffledArray))
        //if we have response but only with empty arrays it means we didnt find any synonyms, else we didnt connect.
        {return <div className={"h-100 row align-items-center"}>
            <div className={'col text-center'}><p>We could not find any synonyms for your words, would you like to</p>
            <button className={'btn btn-dark ml-3'} onClick={this.handlereturn}>Try again?</button></div></div>}
            else {return <div className={'h-100 row align-items-center'}>
            <div className={'col text-center'}> Connection problem</div></div>}
    }
}

export default withRouter(ReturnView)