import React from 'react'
import {withRouter} from 'react-router'
import {connect} from 'react-redux'
import * as actions from '../redux/actions'
import {bindActionCreators} from 'redux'
import Timer from './timer'
import separation from '../logic/separation'
import ReturnView from './returnView'

class Output extends React.Component {
    constructor(props) {
        super(props);
        let separatedData = separation(this.props.output, this.props.input);
        //assigning inactive className to mapped array. Later we assign it to this element state along with response.
        //timer is running and passed to children prop.
        this.state = {shuffledArray: separatedData.shuffledArray, inputArr: separatedData.options, running_timer: true};
    }

    //on change of redux state we update state to props.
    componentWillReceiveProps(nextProps) {
        let sepData = separation(nextProps.output, nextProps.input);
        this.setState({shuffledArray: sepData.shuffledArray, inputArr: sepData.options});
    }

    //we check if selection is equal to result array parameter word and assign active class if true.
    //if all elements class are true - set timer to false, stop it and pass to timer info that timer needs to stop
    checkIfCorrect = (el, k) => {
        let toggleState = this.state.shuffledArray;
        if (el === this.state.shuffledArray[k].word) {
            toggleState[k].status = 'active';
        }
        else {
            toggleState[k].status = 'inactive';
        }
        let check = (el) => {
            return el.status === 'active'
        };
        if (toggleState.every(check) === true) {
            this.setState({running_timer: false})
        }
        this.setState({shuffledArray: toggleState});
    };

    render() {
        //if our input is valid and we have synonyms for at least one word
        if ((this.state.inputArr.length > 0) && (Array.isArray(this.state.shuffledArray))) {
            return <div className={'container bg-dark text-light minheight'}>
                <h1 className={'pt-5 border-bottom border-light pb-5 text-center'}>Match synonyms!</h1>
                {this.state.shuffledArray.map((arr, i) => {
                    let k = i;
                    if (arr.data.length > 0) {
                        return <div key={i} className={'border-bottom border-light'}>
                            <p key={i}>{arr.data.map((el) => {
                                //we join all words from our synonyms array in one string and make them start with Capital
                            return el.word.charAt(0).toUpperCase()+el.word.slice(1)
                        }).join(', ')}
                        </p>
                            <div className={'form-group ' + this.state.shuffledArray[k].status}><select
                                onChange={(el) => {
                                    this.checkIfCorrect(el.target.value, k)
                                }} className={'form-control ' + this.state.shuffledArray[k].status}>
                                <option>Select</option>
                                {this.state.inputArr.map((el) => {
                                    return <option key={el}>{el}</option>
                                })}</select>
                            </div>
                        </div>
                    }
                    else {
                        return <div key={i}><p></p><h3>Sorry, no synonym for {arr.word}</h3><p></p></div>
                    }
                })}
                <Timer running={this.state.running_timer} difficulty={this.state.inputArr.length}/>
            </div>
        }
        else {
            //we pass to ReturnView our shuffledArray so it can decide what error message to show
            return <ReturnView shuffledArray={this.state.shuffledArray}/>
        }
    }
}

const mapDispatchToProps = dispatch => {
    return {actions: bindActionCreators(actions, dispatch)}
};
const mapStateToProps = (state) => {
    return {input: state.input_words, output: state.result_words}
};
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Output))