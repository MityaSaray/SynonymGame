const shuffle = (array) => {
    let array1 = [...array];
    if (array1.length > 1) {
        let m = array1.length, j, i;
        while (m) {
            i = Math.floor(Math.random() * m--);
            j = array1[m];
            array1[m] = array1[i];
            array1[i] = j;
        }
    }
    return array1
};
export default shuffle;