export default (output, input) => {
    //we separate our array of elements that had synonyms from those who didnt.Array who didnt are active from start
    //and dont take part in a game or scoring
    let valueArray = output.filter(el => el.data.length > 0);
    let val = valueArray.map((el) => {
        return {data: el.data, status: 'inactive', word: el.word}
    });
    let novalueArray = output.filter(el => el.data.length === 0);
    let noval = novalueArray.map((el) => {
        return {data: el.data, status: 'active', word: el.word}
    });
    let optionsArr = input;
    //if there are empty synonym arrays we remove them from options array because there is no reason to propose them
    //as options.
    if (Array.isArray(novalueArray)&&Array.isArray(optionsArr)) {
        optionsArr = optionsArr.filter(word => {
            return novalueArray.every(el => {
                return el.word !== word
            })
        })
    }
    return {shuffledArray: val.concat(noval), options: optionsArr};
}