import {render} from 'react-dom'
import $ from 'jquery';
import "../node_modules/bootstrap/dist/css/bootstrap.css"
import '../node_modules/bootstrap/dist/js/bootstrap.js'
import '../css/main.css'
import React from 'react';
import MainRouter from './mainrouter'
import {Provider} from 'react-redux'
import store from './store'

//TODO: implement data visualization of your result
//TODO: improve design
//TODO: make a modal window for result
render(<Provider store={store}><MainRouter store={store}/></Provider>, document.getElementById("content"));
