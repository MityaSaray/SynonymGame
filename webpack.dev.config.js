const webpack = require('webpack');
const path = require('path');
const config = {
    entry: ['babel-polyfill',
        './js/main.js'],
    output: {
        filename: './release/bundle.js',
        path: path.resolve(__dirname, ''),
        publicPath: '/'
    },

    module: {
        rules: [{test: /\.css$/, loader: 'style-loader!css-loader'},
            {test: /\.jsx$/, loader: 'babel-loader', exclude: /node_modules/},
            {
                test: /\.js$/, exclude: path.resolve(__dirname, "node_modules"),
                use: [{loader: "babel-loader", options: {presets: ["es2015", 'react', 'stage-2']}}]
            },
            {
                test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
                loader: 'file-loader',
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                use: [
                    'file-loader',
                    {
                        loader: 'image-webpack-loader',
                        options: {
                            mozjpeg:{progressive: true, quality: 50},
                            bypassOnDebug: true,
                        },
                    }]}
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jquery: "jquery",
            "window.jQuery": "jquery",
            jQuery: "jquery",
            Popper: ['popper.js', 'default'],
        }),
    ]


};
module.exports = config;