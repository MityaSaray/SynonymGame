const express = require('express');
const app = express();
const cors = require('cors');
const path = require('path');
const mongoClient = require('mongodb').MongoClient;
const bodyParser = require('body-parser');
const mongoURL = 'mongodb://PublicUser:Password@ds251988.mlab.com:51988/my_test_database';
//serve static
app.use(express.static(path.join(__dirname, './')));
//setting gzip on response for JS files request.
app.get('*.js', function (req, res, next) {
    req.url = req.url + '.gz';
    res.set('Content-Encoding', 'gzip');
    res.set('Content-Type', 'text/javascript');
    console.log(req.url);
    res.sendFile(path.join(__dirname, req.url));
});
//using CORS and bodyParser - required to parse incoming JSON data.
app.use(cors());
app.use(bodyParser.json());
//MongoDB connection. We are taking all entries with query string difficulty and sort them by time ascending,limit 10 entries
const options = {connectTimeoutMS: 5000};
mongoClient.connect(mongoURL, options , (err, db) => {
    //if error we throw error
    if (err) throw err;
    let dbase = db.db('my_test_database');
    //create our leaderboard collection if nonexistent
    dbase.createCollection('wn', {
        size: 5242880
    });
    //create log collection if nonexistent
    dbase.createCollection('log', {
        size: 3121440
    });
    //if we refresh our page in middle of application to avoid error due to lack of state we redirect
    app.get('/res/', (req, res) => {
        res.redirect('/')
    });
    //leaderboard get handler, we retrieve data for each sent request sorted by time and with limit of 10 words
    app.get('/leaderboard', (req, res) => {
        dbase.collection('wn').aggregate([{
            $match: {
                diff: parseInt(req.query.diff)
            }
        }, {$project: {_id: 0}}, {$sort: {time: 1}}, {$limit: 10}]).toArray((err, data) => {
            if (err) {
                console.log(err)
            }
            else {
                res.send(data);
            }
            res.end()
        })
    });
    //handling posting result from game end.
    app.post('/res/result/', (req, res) => {
        req.body.diff = parseInt(req.body.diff);
        req.body.time = parseInt(req.body.time);
        dbase.collection('wn').insertOne(req.body);
        dbase.collection('log').insertOne({name: req.body.username, data: new Date()});
        res.end()
    });
    const port = process.env.PORT || 5000;
    app.listen({port: port});
    console.log('app listening to ' + port);
});